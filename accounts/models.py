from django.db import models
from django import forms

# Create your models here.
class Login(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget = forms.PasswordInput)

class Signup(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length = 150, widget = forms.PasswordInput())
    password_confirmation = forms.CharField(max_length = 150, widget = forms.PasswordInput())
